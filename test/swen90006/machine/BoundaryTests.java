package swen90006.machine;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;

import static org.junit.Assert.*;


public class BoundaryTests {
    /*
     * Any method annotated with "@Before" will be executed before each test,
     * allowing the tester to set up some shared resources.
     **/
    @Before
    public void setUp() {
    }

    /*
     * Any method annotated with "@After" will be executed after each test,
     * allowing the tester to release any shared resources used in the setup.
     **/
    @After
    public void tearDown() {
    }


    //******************************* EC1 ********************************
    /*
     * boundary : no RET instruction
     * on point : no RET instruction
     */
    @Test(expected = NoReturnValueException.class)
    public void onPointEC1Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 10");
        machineExeLines(lines);
    }

    /*
     * boundary : no RET instruction
     * off point : one RET instruction
     */
    @Test
    public void offPointEC1Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("RET R0");
        machineExeLines(lines);
        assertEquals(0, machineExeLines(lines));
    }


    //******************************* EC2 ********************************
    /*
     * boundary : more than one RET instructions
     * on point : two RET instruction
     */
    @Test
    public void onPointEC2Test() {
        List<String> lines = new ArrayList<String>();
        final int expected = -10;
        lines.add("MOV R1 " + expected);
        lines.add("RET R1");
        lines.add("MOV R2 " + expected * 10);
        lines.add("RET R2");
        assertEquals(expected, machineExeLines(lines));
    }


    //******************************* EC3 ********************************
    /*
     * boundary: i < 0
     * on point: i = 0
     * */
    @Test
    public void onPointEC3Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 0");
        lines.add("RET R0");
        assertEquals(0, machineExeLines(lines));
    }

    /*
     * boundary: i < 0
     * off point: i = -1
     * */
    @Test(expected = InvalidInstructionException.class)
    public void offPointEC3Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R-1 0");
        machineExeLines(lines);
    }


    //******************************* EC4 ********************************
    /*
     * boundary: i > 31
     * on point: i = 31
     * */
    @Test
    public void onPointEC4Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R31 -2");
        lines.add("RET R31");
        assertEquals(-2, machineExeLines(lines));
    }

    /*
     * boundary: i > 31
     * off point: i = 32
     * */
    @Test(expected = InvalidInstructionException.class)
    public void offPointEC4Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R32 0");
        machineExeLines(lines);
    }


    //******************************* EC5 ********************************
    /*
     * boundary: val < -65535
     * on point: val = -65535
     * */
    @Test
    public void onPointEC5Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 -65535");
        lines.add("RET R1");
        assertEquals(-65535, machineExeLines(lines));
    }

    /*
     * boundary: val < -65535
     * off point: val = -65536
     * */
    @Test(expected = InvalidInstructionException.class)
    public void offPointEC5Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 -65536");
        lines.add("RET R1");
        machineExeLines(lines);
    }


    //******************************* EC6 ********************************
    /*
     * boundary: val > 65535
     * on point: val = 65535
     * */
    @Test
    public void onPointEC6Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 65535");
        lines.add("RET R1");
        assertEquals(65535, machineExeLines(lines));
    }

    /*
     * boundary: val > 65535
     * off point: val = 65536
     * */
    @Test(expected = InvalidInstructionException.class)
    public void offPointEC6Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 65536");
        lines.add("RET R1");
        machineExeLines(lines);
    }


    //******************************* EC7 ********************************
    /*
     * boundary: regV <= -(2^31)
     * on point: regV = -(2^31)
     * */

    /*
     * boundary: regV <= -(2^31)
     * off point: regV = -(2^31 + 1)
     * */

    //EC7 cannot give a test


    //******************************* EC8 ********************************
    /*
     * boundary: regV >= 2^31
     * on point: regV = 2^31
     * */

    /*
     * boundary: regV >= 2^31
     * off point: regV = 2^31 + 1
     * */

    //EC8 cannot give a test


    //******************************* EC9 ********************************
    /*
     * boundary: ADD > 0
     * off point: one ADD
     * */
    @Test
    public void offPointEC9Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 1");
        lines.add("ADD R1 R1 R1");
        lines.add("RET R1");
        assertEquals("Wrong ADD result.", 2, machineExeLines(lines));
    }


    //******************************* EC10 ********************************
    /*
     * boundary: SUB > 0
     * off point: one SUB
     * */
    @Test
    public void offPointEC10Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 1");
        lines.add("SUB R1 R1 R1");
        lines.add("RET R1");
        assertEquals("Wrong SUB result.", 0, machineExeLines(lines));
    }


    //******************************* EC11 ********************************
    /*
     * boundary: MUL > 0
     * off point: one MUL
     * */
    @Test
    public void offPointEC11Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 1");
        lines.add("MUL R1 R1 R1");
        lines.add("RET R1");
        assertEquals("Wrong MUL result.", 1, machineExeLines(lines));
    }


    //******************************* EC12 ********************************
    /*
     * boundary: div by 0
     * on point: divisor = 0
     * */
    @Test
    public void onPointEC12Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 0");
        lines.add("MOV R0 3");
        lines.add("DIV R0 R0 R1");
        lines.add("RET R0");
        assertEquals(3, machineExeLines(lines));
    }

    /*
     * boundary: div by 0
     * off point one: divisor = 1
     * return a normal value
     * */
    @Test
    public void offPointOneEC12Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 1");
        lines.add("MOV R0 -3");
        lines.add("DIV R0 R0 R1");
        lines.add("RET R0");
        assertEquals(-3, machineExeLines(lines));
    }

    /*
     * boundary: div by 0
     * off point two: divisor = -1
     * return a normal value
     * */
    @Test
    public void offPointTwoEC12Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R1 -1");
        lines.add("MOV R0 3");
        lines.add("DIV R0 R0 R1");
        lines.add("RET R0");
        assertEquals(-3, machineExeLines(lines));

    }


    //EC13 has no test


    //******************************* EC14 ********************************
    /*
     * boundary: MOV > 0
     * on point: one MOV
     * */
    @Test
    public void onPointEC14Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 1");
        lines.add("RET R0");
        assertEquals("Wrong ADD result.", 1, machineExeLines(lines));
    }


    //******************************* EC15 ********************************
    /*
     * boundary: RET undefined reg
     * on point: RET undefined reg
     * return a normal value
     * */
    @Test
    public void onPointEC15Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("RET R0");
        assertEquals(0, machineExeLines(lines));
    }

    /*
     * boundary: RET undefined reg
     * off point: RET defined reg
     * return a normal value
     * */
    @Test
    public void offPointEC15Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 1");
        lines.add("RET R0");
        assertEquals("Wrong RET result.", 1, machineExeLines(lines));
    }


    //EC16 has no test


    //******************************* EC17 ********************************
    /*
     * test for JMP
     * boundary: val = 0
     * on point: val = 0
     * cause an infinite loop
     * but can not have a test case since it will loop forever
     * */


    //******************************* EC18 ********************************
    /*
     * boundary: val < 0 && |val| > _pc
     * off point one: val = -1 && _pc = 0
     * throw the NoReturnValueException
     * */
    @Test(expected = NoReturnValueException.class)
    public void offPointEC18Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("JMP -1");
        lines.add("RET R0");
        machineExeLines(lines);
    }


    //******************************* EC19 ********************************
    /*
     * boundary: val > pc_ && val > 0
     * off point one: val = 1 && _pc = 0
     * throw the NoReturnValueException
     * */
    @Test(expected = NoReturnValueException.class)
    public void offPointEC19Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("JMP 1");
        machineExeLines(lines);
    }


    //******************************* EC20 ********************************
    /*
     * boundary: _pc <= v <= pc_ && v != 0
     * off point: v = pc_ = _pc = 1
     * return a normal value
     * */
    @Test
    public void offPointEC20Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 1");
        lines.add("JMP 1");
        lines.add("MOV R0 2");
        lines.add("RET R0");
        assertEquals("Wrong JMP result.", 2, machineExeLines(lines));
    }


    //******************************* EC21 ********************************
    /*
     * test for JZ
     * boundary: val = 0
     * on point: val = 0
     * cause an infinite loop
     * but can not have a test case since it will loop forever
     * */


    //******************************* EC22 ********************************
    /*
     * boundary: val < 0 && |val| > _pc
     * off point one: val = -1 && _pc = 0
     * throw the NoReturnValueException
     * */
    @Test(expected = NoReturnValueException.class)
    public void offPointEC22Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("JZ R0 -1");
        lines.add("RET R0");
        machineExeLines(lines);
    }


    //******************************* EC23 ********************************
    /*
     * boundary: val > pc_ && val > 0
     * off point one: val = 1 && _pc = 0
     * throw the NoReturnValueException
     * */
    @Test(expected = NoReturnValueException.class)
    public void offPointEC23Test()
            throws Throwable {
        List<String> lines = new ArrayList<String>();
        lines.add("JZ R0 1");
        machineExeLines(lines);
    }


    //******************************* EC24 ********************************
    /*
     * boundary: _pc <= v <= pc_ && v != 0
     * off point: v = pc_ = _pc = 1
     * return a normal value
     * */
    @Test
    public void offPointEC24Test() {
        List<String> lines = new ArrayList<String>();
        lines.add("MOV R0 1");
        lines.add("JZ R0 3");
        lines.add("SUB R0 R0 R0");
        lines.add("MOV R0 2");
        lines.add("RET R0");
        assertEquals("Wrong JMP result.", 2, machineExeLines(lines));
    }


    //******************************* EC25 ********************************
    /*
     * test for LDR
     * boundary: b + v <= -(2^31)
     * on point: b + v = -(2^31)
     * stop the programme and print out the error
     * /

    /*
     * boundary: b + v <= -(2^31)
     * off point: b + v = -(2^31 + 1)
     * stop the programme and print out the error
     * */

    //EC25 cannot give a test


    //******************************* EC26 ********************************
    /*
     * test for LDR
     * boundary: b + v in -(2^31-1) ... (2^31-1)
     * on point one: b + v = -(2^31-1)
     * return a normal value
     * */

    /*
     * test for LDR
     * boundary: b + v in -(2^31-1) ... (2^31-1)
     * on point two: b + v = (2^31-1)
     * return a normal value
     * */


    //******************************* EC27 ********************************
    /*
     * test for LDR
     * boundary: b + v >= 2^31
     * on point: b + v = 2^31
     * stop the programme and print out the error
     */

    /*
     * test for LDR
     * boundary: b + v >= 2^31
     * off point: b + v = 2^31 + 1
     * stop the programme and print out the error
     */

    //EC27 can not test here


    //******************************* EC28 ********************************
    /*
     * test for STR
     * boundary: b + v <= -(2^31)
     * on point: b + v = -(2^31)
     * stop the programme and print out the error
     * /

    /*
     * boundary: b + v <= -(2^31)
     * off point: b + v = -(2^31 + 1)
     * stop the programme and print out the error
     * */

    //EC28 cannot give a test


    //******************************* EC29 ********************************
    /*
     * test for STR
     * boundary: b + v in -(2^31-1) ... (2^31-1)
     * on point one: b + v = -(2^31-1)
     * return a normal value
     * */

    /*
     * test for STR
     * boundary: b + v in -(2^31-1) ... (2^31-1)
     * on point two: b + v = (2^31-1)
     * return a normal value
     * */


    //******************************* EC30 ********************************
    /*
     * test for STR
     * boundary: b + v >= 2^31
     * on point: b + v = 2^31
     * stop the programme and print out the error
     */

    /*
     * test for STR
     * boundary: b + v >= 2^31
     * off point: b + v = 2^31 + 1
     * stop the programme and print out the error
     */

    //EC30 can not test here


    /*
     *  Machine executes input lines
     * */
    private int machineExeLines(List<String> lines) {
        Machine m = new Machine();
        return m.execute(lines);
    }
}